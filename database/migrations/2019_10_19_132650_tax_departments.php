<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TaxDepartments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_departments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('code');
            $table->string('name');
            $table->string('bin');
            $table->string('type', 20);
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['code', 'deleted_at']);
            $table->unique(['name', 'deleted_at']);
            $table->unique(['bin', 'deleted_at']);

            $table->foreign('parent_id')
                ->references('id')
                ->on('tax_departments');
        });

        $referenceGroupId = DB::table('right_groups')->insertGetId([
            'name' => 'references',
            'description' => 'references_right_group_description',
        ]);

        DB::table('rights')->insert([
            [
                'name' => 'taxations',
                'actions' => json_encode(['index', 'store', 'show', 'update', 'destroy']),
                'group_id' => $referenceGroupId,
                'description' => 'taxations_right_description',
            ],
            [
                'name' => 'tax_departments',
                'actions' => json_encode(['index', 'store', 'show', 'update', 'destroy']),
                'group_id' => $referenceGroupId,
                'description' => 'tax_departments_right_description',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('rights')->whereIn('name', ['taxations', 'tax_departments'])->delete();

        DB::table('right_groups')->where('name', 'references')->delete();

        Schema::dropIfExists('tax_departments');
    }
}
