<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddUsersRight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('rights')->insert([
            'name' => 'users',
            'actions' => json_encode([
                'index', 'store', 'show', 'update', 'destroy'
            ]),
            'group_id' => DB::table('right_groups')->where('name', 'system')->value('id')
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('rights')->where('name', 'users')->delete();
    }
}
