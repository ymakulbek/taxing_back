<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->jsonb('value');
            $table->boolean('system')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('settings')->insert([
            ['name' => 'minimal_salary', 'value' => '42500', 'system' => true, 'created_at' => Carbon::now()],
            [
                'name' => 'phone',
                'value' => json_encode(['codes' => ['701', '702', '705', '707', '775', '778']]),
                'system' => true,
                'created_at' => Carbon::now()
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
