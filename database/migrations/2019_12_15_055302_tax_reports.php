<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TaxReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description');
            $table->string('identification_number_type')->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('organization_types')->insert([
            ['name' => 'ip', 'description' => 'ip_description', 'identification_number_type' => 'iin'],
            ['name' => 'too', 'description' => 'too_description', 'identification_number_type' => 'bin'],
        ]);

        Schema::create('organizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('organization_type_id');
            $table->string('identification_number', 12)->unique();
            $table->unsignedBigInteger('registration_department_id');
            $table->unsignedBigInteger('actual_department_id');
            $table->unsignedBigInteger('taxation_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['id', 'organization_type_id']);

            $table->foreign('organization_type_id')
                ->references('id')
                ->on('organization_types');
            $table->foreign('registration_department_id')
                ->references('id')
                ->on('tax_departments');
            $table->foreign('actual_department_id')
                ->references('id')
                ->on('tax_departments');
            $table->foreign('taxation_id')
                ->references('id')
                ->on('taxations');
        });

        DB::table('organizations')->insert([
            'name' => 'I-COMMERCE',
            'organization_type_id' => 1,
            'identification_number' => '880125301713',
            'registration_department_id' => 2,
            'actual_department_id' => 2,
            'taxation_id' => 1,
        ]);

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('registration_department_id');
            $table->dropColumn('actual_department_id');
            $table->dropColumn('taxation_id');

            $table->unsignedBigInteger('organization_id')->nullable();

            $table->foreign('organization_id')
                ->references('id')
                ->on('organizations');
        });

        DB::table('users')->where('id', 1)->update(['organization_id' => 1]);

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('organization_id')->nullable(false)->change();
        });

        Schema::create('tax_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('xml');
            $table->text('signature');
            $table->string('status', 50);
            $table->unsignedBigInteger('organization_id');
            $table->timestamp('acceptance_date')->nullable();
            $table->jsonb('meta');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('organization_id')
                ->references('id')
                ->on('organizations');
        });

        $reportsGroupId = DB::table('right_groups')->insertGetId([
            'name' => 'taxing_reports',
            'description' => 'taxing_reports_description',
        ]);

        DB::table('rights')->insert([
            'name' => 'submit_report',
            'actions' => json_encode(['submit']),
            'group_id' => $reportsGroupId,
            'description' => 'submit_report_right_description'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('rights')->where('name', 'submit_report')->delete();
        DB::table('right_groups')->where('name', 'taxing_reports')->delete();

        Schema::dropIfExists('tax_reports');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('organization_id');

            $table->unsignedBigInteger('registration_department_id')->nullable();
            $table->unsignedBigInteger('actual_department_id')->nullable();
            $table->unsignedBigInteger('taxation_id')->nullable();

            $table->foreign('registration_department_id')
                ->references('id')
                ->on('tax_departments');
            $table->foreign('actual_department_id')
                ->references('id')
                ->on('tax_departments');
            $table->foreign('taxation_id')
                ->references('id')
                ->on('taxations');
        });

        Schema::dropIfExists('organizations');
        Schema::dropIfExists('organization_types');
    }
}
