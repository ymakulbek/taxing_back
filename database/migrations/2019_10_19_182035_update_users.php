<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class UpdateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('phone_id');
            $table->unsignedBigInteger('registration_department_id');
            $table->unsignedBigInteger('actual_department_id');
            $table->unsignedBigInteger('taxation_id');
            $table->string('iin', 12);
            $table->string('surname');
            $table->string('patronymic')->nullable();
            $table->boolean('resident');
            $table->softDeletes();

            $table->unique(['name', 'surname', 'patronymic', 'deleted_at']);
            $table->unique(['iin', 'deleted_at']);

            $table
                ->foreign('role_id')
                ->references('id')
                ->on('roles');

            $table
                ->foreign('phone_id')
                ->references('id')
                ->on('phones');

            $table
                ->foreign('registration_department_id')
                ->references('id')
                ->on('tax_departments');

            $table
                ->foreign('actual_department_id')
                ->references('id')
                ->on('tax_departments');

            $table
                ->foreign('taxation_id')
                ->references('id')
                ->on('taxations');
        });

        DB::table('roles')->insert([
            'name' => 'client', 'description' => 'client_role_description', 'created_at' => Carbon::now()
        ]);

        $superuserRoleId = DB::table('roles')->insertGetId([
            'name' => 'superuser', 'description' => 'superuser_role_description', 'created_at' => Carbon::now()
        ]);

        $phoneId = DB::table('phones')->insertGetId([
            'code' => 778,
            'number' => '7415310'
        ]);

        $simplifiedTaxationId = DB::table('taxations')->insertGetId([
            'code' => 910,
            'name' => 'simplified_taxation',
            'description' => 'simplified_taxation_description',
        ]);

        $almatyTaxDepartmentId = DB::table('tax_departments')->insertGetId([
            'code' => 6001,
            'name' => 'ДГД по г.Алматы',
            'type' => 'city',
            'bin' => '141140001547'
        ]);

        $zhetisuTaxDepartmentId = DB::table('tax_departments')->insertGetId([
            'code' => 6005,
            'name' => 'УГД по Жетысускому району',
            'type' => 'district',
            'bin' => '600500000015',
            'parent_id' => $almatyTaxDepartmentId,
        ]);

        DB::table('users')->insert([
            'role_id' => $superuserRoleId,
            'email' => 'ermmak@taxing.kz',
            'name' => 'Yermek',
            'surname' => 'Makulbek',
            'patronymic' => 'Zharkenuly',
            'iin' => '880125301713',
            'password' => Hash::make('12345'),
            'resident' => true,
            'phone_id' => $phoneId,
            'taxation_id' => $simplifiedTaxationId,
            'registration_department_id' => $zhetisuTaxDepartmentId,
            'actual_department_id' => $zhetisuTaxDepartmentId,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->where('iin', '880125301713')->delete();

        DB::table('tax_departments')->whereIn('bin', ['141140001547', '600500000015'])->delete();

        DB::table('taxations')->where('code', 910)->delete();

        DB::table('phones')->where('number', '7415310')->delete();

        DB::table('roles')->where('name', 'superuser')->delete();

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('actual_department_id');
            $table->dropColumn('registration_department_id');
            $table->dropColumn('phone_id');
            $table->dropColumn('taxation_id');
            $table->dropColumn('resident');
            $table->dropColumn('patronymic');
            $table->dropColumn('surname');
            $table->dropColumn('iin');
            $table->dropColumn('role_id');
            $table->dropSoftDeletes();
        });
    }
}
