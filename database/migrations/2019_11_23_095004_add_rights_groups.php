<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddRightsGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $systemGroupId = DB::table('right_groups')->insertGetId([
            'name' => 'system',
            'description' => 'system_group_description'
        ]);

        DB::table('rights')->insert([
            'name' => 'roles',
            'description' => 'roles_right_description',
            'group_id' => $systemGroupId,
            'actions' => json_encode([
                'index', 'store', 'show', 'update', 'delete'
            ])
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('rights')->where('name', 'roles')->delete();

        DB::table('right_groups')->where('name', 'system')->delete();
    }
}
