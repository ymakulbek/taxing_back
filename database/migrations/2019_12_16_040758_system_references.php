<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SystemReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('rights')->insert([
            'name' => 'system_references',
            'actions' => json_encode(['index', 'mrp', 'mzp', 'mobile_operators']),
            'group_id' => 1,
            'description' => 'system_references_right_description',
        ]);

        DB::table('settings')->insert([
            'name' => 'monthly_calculation_indicator',
            'system' => true,
            'value' => json_encode([
                ['year' => '2018', 'amount' => 2405],
                ['year' => '2019', 'amount' => 2525],
                ['year' => '2020', 'amount' => 2651],
            ])
        ]);

        DB::table('settings')->where('name', 'minimal_salary')->update([
            'value' => json_encode([
                ['year' => '2018', 'amount' => 42500],
                ['year' => '2018', 'amount' => 42500],
                ['year' => '2018', 'amount' => 28284],
            ])
        ]);

        DB::table('settings')->where('name', 'phone')->update([
            'name' => 'mobile_operators', 'value' => json_encode([
                ['name' => 'Altel', 'codes' => [700, 708]],
                ['name' => 'Kcell', 'codes' => [701, 702, 775, 778]],
                ['name' => 'Beeline', 'codes' => [705, 771, 776, 777]],
                ['name' => 'Tele2', 'codes' => [707, 747]],
            ])
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('settings')->where('name', 'monthly_calculation_indicator')->delete();
        DB::table('rights')->where('name', 'system_references')->delete();
    }
}
