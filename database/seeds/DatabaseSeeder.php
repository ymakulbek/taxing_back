<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PhonesSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(TaxationsSeeder::class);
        $this->call(TaxDepartmentsSeeder::class);
        $this->call(UsersSeeder::class);
    }
}
