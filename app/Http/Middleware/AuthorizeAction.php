<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class AuthorizeAction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->route()->getName() && $this->authorize($request);

        return $next($request);
    }

    /**
     * Authorize
     * @param Request $request
     */
    public function authorize(Request $request)
    {
        $route = $request->route();

        [$resource, $action] = explode('.', $route->getName());

        $routeParams = $route->parameters();

        $args = empty($routeParams) ? $action : array_merge([$action], array_values($route->parameters()));

        Gate::authorize($resource, $args);
    }
}
