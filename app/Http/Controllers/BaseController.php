<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;

/**
 * Class BaseController
 * @package App\Http\Controllers
 */
class BaseController extends Controller
{
    /**
     * @var int|string
     */
    protected $page;

    /**
     * @var int|string
     */
    protected $itemsPerPage;

    /**
     * @var array|string
     */
    protected $sortBy;

    /**
     * BaseController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->setCommonParams($request);
    }

    /**
     * Set common params
     * @param Request $request
     */
    protected function setCommonParams(Request $request)
    {
        $validated = $request->validate($this->paramsRules());

        $this->page = $validated['page'] ?? 1;
        $this->itemsPerPage = $validated['itemsPerPage'] ?? 15;

        isset($validated['sortBy']) && $this->sortBy = $validated['sortBy'];
    }

    /**
     * Common params rules
     * @return array
     */
    protected function paramsRules(): array
    {
        return [
            'page' => ['integer', 'min:1'],
            'itemsPerPage' => ['integer', 'min:-1'],
            'sortBy' => function ($attribute, $value, $fail) {
                ! $this->validSortBy($value) && $fail('invalid_sort_by');
            }
        ];
    }

    /**
     * Check if sort by is valid
     * @param $value
     * @return bool|false|int
     */
    protected function validSortBy($value)
    {
        $regex = '/^\w+(:asc|:desc)?$/';

        return is_array($value)
            ? collect($value)->every(function ($item) use ($regex) { return preg_match($regex, $item); })
            : preg_match($regex, $value);
    }
}