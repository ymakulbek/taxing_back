<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Requests\TaxationRequest;
use App\Repositories\SortByCriteria;
use App\Repositories\Taxations\Taxation;
use App\Repositories\Users\User;

class TaxationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param Taxation $repository
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index(Taxation $repository)
    {
        return $repository
            ->pushCriteria(SortByCriteria::sortBy($this->sortBy))
            ->paginate($this->page, $this->itemsPerPage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaxationRequest $request
     * @param Taxation $repository
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TaxationRequest $request, Taxation $repository)
    {
        return response()->json(
            !! $repository->insert($request->validationData())
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaxationRequest $request
     * @param Taxation $repository
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TaxationRequest $request, Taxation $repository, $id)
    {
        $data = collect($request->validationData())->filter(function ($value, $key) {
            return $key !== '_method';
        });

        return response()->json(
            !! $repository->where('id', $id)->update($request->validationData())
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Taxation $repository
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Taxation $repository, $id)
    {
        $hasBoundUser = !! (new User)->where('taxation_id', $id)->count();

        $hasBoundUser && abort(409, 'has_bound_user');

        return response()->json($repository->delete($id));
    }
}
