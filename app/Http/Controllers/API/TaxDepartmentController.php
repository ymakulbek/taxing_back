<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Requests\TaxDepartmentRequest;
use App\Repositories\SortByCriteria;
use App\Repositories\TaxDepartments\TaxDepartment;
use App\Repositories\TaxDepartments\TaxDepartmentCriteria;
use App\Repositories\Users\User;

/**
 * Class TaxDepartmentController
 * @package App\Http\Controllers\API
 */
class TaxDepartmentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param TaxDepartment $repository
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index(TaxDepartment $repository)
    {
        return $repository
            ->pushCriteria(
                (new TaxDepartmentCriteria)->setForeignKey('parent_id')->setSelectAs('parent')
            )
            ->pushCriteria(SortByCriteria::sortBy($this->sortBy))
            ->paginate($this->page, $this->itemsPerPage);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaxDepartmentRequest $request
     * @param TaxDepartment $repository
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TaxDepartmentRequest $request, TaxDepartment $repository)
    {
        return response()->json(
            !! $repository->insert($request->data())
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaxDepartmentRequest $request
     * @param TaxDepartment $repository
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TaxDepartmentRequest $request, TaxDepartment $repository, $id)
    {
        return response()->json(
            !! $repository->where('id', $id)->update($request->data())
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param TaxDepartment $repository
     * @param User $userRepository
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(TaxDepartment $repository, User $userRepository, $id)
    {
        $hasBoundUser = !! $userRepository
            ->where('registration_department_id', $id)
            ->orWhere('actual_department_id', $id)
            ->count();

        $hasBoundUser && abort(409, 'has_bound_user');

        $hasBoundDepartment = !! $repository->where('parent_id', $id)->count();

        $hasBoundDepartment && abort(409, 'has_child_department');

        return response()->json(!! $repository->skipCriteria()->delete($id));
    }
}
