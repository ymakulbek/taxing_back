<?php

namespace App\Http\Controllers\API;

use App\Facades\System\Settings;
use App\Http\Controllers\BaseController;
use App\Http\Requests\UserRequest;
use App\Repositories\Phones\Phone;
use App\Repositories\Phones\PhoneCriteria;
use App\Repositories\Roles\RoleCriteria;
use App\Repositories\Taxations\Taxation;
use App\Repositories\TaxDepartments\TaxDepartment;
use App\Repositories\Users\User;
use Illuminate\Support\Facades\DB;

class UserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param User $repository
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function index(User $repository)
    {
        return $repository->where('id', '>', 1)
            ->pushCriteria(new RoleCriteria)
            ->pushCriteria(new PhoneCriteria)
            ->paginate($this->page, $this->itemsPerPage);
    }

    /**
     * Taxations list
     * @param Taxation $repository
     * @return mixed
     */
    public function taxations(Taxation $repository)
    {
        return $repository->all();
    }

    /**
     * Tax departments list
     * @param TaxDepartment $repository
     * @return mixed
     */
    public function taxDepartments(TaxDepartment $repository)
    {
        return $repository->all();
    }

    /**
     * Phone operators codes list
     * @return mixed
     */
    public function mobileOperators()
    {
        return Settings::get('mobile_operators', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @param User $repository
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserRequest $request, User $repository)
    {
        return response()->json(
            DB::transaction(function () use ($request, $repository) {
                $phoneId = $this->savePhone($request->validationData()['phone']);

                return !! $repository->insert($request->data($phoneId));
            })
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param User $repository
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserRequest $request, User $repository, $id)
    {
        return response()->json(
            DB::transaction(function () use ($request, $repository, $id) {
                $phoneSaved = $this->savePhone(
                    $request->validationData()['phone'],
                    $repository->find($id)->phone_id
                );

                $userSaved = $repository->where('id', $id)->update($request->data());

                return $phoneSaved || $userSaved;
            })
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $repository
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $repository, $id)
    {
        // todo check bound items
        return response()->json(
            !! $repository->delete($id)
        );
    }

    /**
     * Save phone
     * @param array $data
     * @param int|null $id
     * @return bool|int
     */
    protected function savePhone(array $data, int $id = null)
    {
        $repository = new Phone;

        return $id
            ? $repository->where('id', $id)->update($data)
            : $repository->insert($data);
    }
}
