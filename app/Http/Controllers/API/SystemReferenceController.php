<?php


namespace App\Http\Controllers\API;


use App\Facades\System\Settings as SettingsFacade;
use App\Http\Controllers\BaseController;
use App\Http\Requests\MobileOperatorsRequest;
use App\Http\Requests\MzrpRequest;
use App\Repositories\Settings\Settings;

class SystemReferenceController extends BaseController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(
            (new Settings)
                ->whereIn('name', ['monthly_calculation_indicator', 'minimal_salary', 'mobile_operators'])
                ->all()
        );
    }

    /**
     * @param MzrpRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mrp(MzrpRequest $request)
    {
        return response()->json(
            SettingsFacade::set('monthly_calculation_indicator', json_encode($request->input('items')))
        );
    }

    /**
     * @param MzrpRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mzp(MzrpRequest $request)
    {
        return response()->json(
            SettingsFacade::set('minimal_salary', json_encode($request->input('items')))
        );
    }

    /**
     * @param MzrpRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function mobileOperators(MobileOperatorsRequest $request)
    {
        return response()->json(
            SettingsFacade::set('mobile_operators', json_encode($request->input('items')))
        );
    }
}