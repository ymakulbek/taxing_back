<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientRegistrationRequest;
use App\Repositories\Phones\Phone;
use App\Repositories\Roles\Role;
use App\Repositories\Users\User;
use Illuminate\Support\Facades\DB;

class RegistrationController extends Controller
{
    /**
     * @param ClientRegistrationRequest $request
     * @param User $repository
     * @return \Illuminate\Http\JsonResponse
     */
    public function client(ClientRegistrationRequest $request, User $repository)
    {
        return response()->json(
            DB::transaction(function () use ($request, $repository) {
                $phoneId = (new Phone)->insert($request->input('phone'));

                $data = array_merge($request->data($phoneId), [
                    'role_id' => (new Role)->where('name', 'client')->first()->id
                ]);

                return !! $repository->insert($data);
            })
        );
    }
}
