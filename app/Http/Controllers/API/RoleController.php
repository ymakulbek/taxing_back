<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\BaseController;
use App\Http\Requests\RoleRequest;
use App\Repositories\GroupByCriteria;
use App\Repositories\Rights\Right;
use App\Repositories\Rights\RightCriteria;
use App\Repositories\RoleRights\RoleRight;
use App\Repositories\RoleRights\RoleRightCriteria;
use App\Repositories\Roles\Role;
use App\Repositories\Users\User;
use Illuminate\Support\Facades\DB;

class RoleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        return $this->withCriteria()->whereNotIn('name', ['client', 'superuser'])->all();
    }

    /**
     * Rights list
     * @return \Illuminate\Support\Collection
     */
    public function rights()
    {
        return (new Right)->where('name', '<>', 'roles')->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RoleRequest $request)
    {
        return response()->json(
            DB::transaction(function () use ($request) {
                $roleId = (new Role)->insert($request->roleData());

                return !! $roleId && (new RoleRight)->insert(
                    $request->rightsData($roleId)
                );
            })
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json(
            $this->withCriteria()
                ->pushCriteria(new RoleRightCriteria)
                ->pushCriteria(GroupByCriteria::groups('roles_rights.id'))
                ->find($id)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RoleRequest $request, $id)
    {
        return response()->json(
            DB::transaction(function () use ($request, $id) {
                $roleUpdated = (new Role)->where('id', $id)->update($request->roleData());

                $roleRightRepository = new RoleRight;

                $roleRightRepository->where('role_id', $id)->delete();

                $rightsInserted = $roleRightRepository->insert($request->rightsData($id));

                return $roleUpdated || $rightsInserted;
            })
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $repository
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Role $repository, $id)
    {
        $hasBoundUsers = !! (new User)->where('role_id', $id)->count();

        $hasBoundUsers && abort(409, 'has_bound_records');

        return response()->json(
            !! $repository->delete($id)
        );
    }

    /**
     * @return \App\Repositories\Repository
     */
    protected function withCriteria()
    {
        return (new Role)
            ->pushCriteria(
                (new RightCriteria)
                    ->setForeignKey('default_right_id')
                    ->setSelectAs('default_right')
            );
    }
}
