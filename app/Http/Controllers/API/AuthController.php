<?php


namespace App\Http\Controllers\API;


use App\Extensions\TaxingUser;
use App\Http\Controllers\BaseController;
use App\Http\Requests\LoginRequest;
use App\Repositories\Organizations\Organization;
use App\Repositories\OrganizationTypes\OrganizationTypeCriteria;
use App\Repositories\RightGroups\RightGroupCriteria;
use App\Repositories\Rights\Right;
use App\Repositories\Taxations\TaxationCriteria;
use App\Repositories\TaxDepartments\TaxDepartmentCriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    /**
     * @param LoginRequest $request
     * @return string
     */
    public function login(LoginRequest $request)
    {
        $credentials = array_merge(
            $request->only('email', 'password'), ['deleted_at' => null]
        );

        if (Auth::attempt($credentials)) {
            return response()->json([
                'token' => Auth::user()->createToken('Taxing Access Client')->accessToken
            ]);
        }

        abort(401, 'incorrect_credentials');
    }

    /**
     * Logout
     * @param Request $request
     */
    public function logout(Request $request)
    {
        $request->user()->token()->delete();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {
        $user = $request->user();

        $this->setOrganization($user);

        $user->isSuperUser() && $this->setAllRights($user);

        return response()->json(['user' => $request->user()]);
    }

    /**
     * Set all rights to the user
     * @param TaxingUser $user
     */
    protected function setAllRights(TaxingUser $user)
    {
        $user->rights = (new Right)->pushCriteria(new RightGroupCriteria)->all();
    }

    /**
     * Set user organization
     * @param TaxingUser $user
     */
    protected function setOrganization(TaxingUser $user)
    {
        $user->organization = (new Organization)
            ->pushCriteria(new OrganizationTypeCriteria)
            ->pushCriteria((new TaxationCriteria))
            ->pushCriteria($this->taxDepartmentCriteria('registration'))
            ->pushCriteria($this->taxDepartmentCriteria('actual'))
            ->find($user->organization_id);
    }

    /**
     * @param string $type
     * @return \App\Repositories\JoinCriteria
     */
    protected function taxDepartmentCriteria(string $type)
    {
        return (new TaxDepartmentCriteria)
            ->setForeignKey($type . '_department_id')
            ->setRightJoinAs($type . '_departments')
            ->setSelectAs($type . '_department');
    }
}