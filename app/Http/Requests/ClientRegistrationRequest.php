<?php


namespace App\Http\Requests;


use Illuminate\Validation\Rule;

/**
 * Class UserRegistrationRequest
 * @package App\Http\Requests
 */
class ClientRegistrationRequest extends UserRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'taxation' => ['required', 'integer', Rule::exists('taxations', 'id')],
            'registration_department' => ['required', 'integer', Rule::exists('tax_departments', 'id')],
            'actual_department' => ['required', 'integer', Rule::exists('tax_departments', 'id')],
        ]);
    }

    /**
     * Persist data
     * @param int|null $phoneId
     * @return array
     */
    public function data(int $phoneId = null): array
    {
        $requestData = array_filter(parent::data($phoneId), function ($key) {
            return ! in_array($key, ['taxation', 'registration_department', 'actual_department']);
        }, ARRAY_FILTER_USE_KEY);

        $data['role_id'] = 1;
        $data['taxation_id'] = $requestData['taxation'];
        $data['actual_department_id'] = $requestData['actual_department'];
        $data['registration_department_id'] = $requestData['registration_department'];

        return $data;
    }
}