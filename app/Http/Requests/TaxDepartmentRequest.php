<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class TaxDepartmentRequest
 * @package App\Http\Requests
 */
class TaxDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'regex:/^[\w\- ]+$/', $this->uniqueRule()],
            'code' => ['required', 'digits:4', $this->uniqueRule()],
            'bin' => ['required', 'digits:12', $this->uniqueRule()],
            'type' => ['required', Rule::in(['region', 'city', 'district'])],
            'parent' => ['integer', Rule::exists('tax_departments', 'id')],
        ];
    }

    /**
     * Tax department store data
     * @return array
     */
    public function data(): array
    {
        return [
            'name' => $this->input('name'),
            'code' => $this->input('code'),
            'bin' => $this->input('bin'),
            'type' => $this->input('type'),
            'parent_id' => $this->input('parent'),
        ];
    }

    /**
     * @return \Illuminate\Validation\Rules\Unique
     */
    protected function uniqueRule()
    {
        return Rule::unique('tax_departments')
            ->ignore($this->route('tax_department'))
            ->whereNull('deleted_at');
    }
}
