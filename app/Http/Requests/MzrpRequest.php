<?php


namespace App\Http\Requests;


class MzrpRequest extends BaseRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        $year = date("Y");
        $maxYear = intval($year) + 1;

        return [
            'items' => ['required', 'array', 'min:1'],
            'items.*.year' => ['required', 'integer', 'digits:4', 'distinct', 'min:2018', "max:$maxYear"],
            'items.*.amount' => ['required', 'integer', 'min:1'],
        ];
    }
}