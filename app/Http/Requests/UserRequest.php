<?php

namespace App\Http\Requests;

use App\Facades\System\Settings;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', $this->nameRegexRule()],
            'surname' => [
                'required',
                $this->nameRegexRule(),
                $this->uniqueRule()
                    ->where('name', $this->input('name'))
                    ->where('patronymic', $this->input('patronymic'))
            ],
            'patronymic' => $this->nameRegexRule(),
            'email' => ['required', 'email', $this->uniqueRule()],
            'iin' => ['required', 'digits:12'],
            'resident' => 'boolean',
            'phone' => ['required', 'array', 'min:1'],
            'phone.code' => [
                'required', 'digits:3', Rule::in($this->phoneCodes())
            ],
            'phone.number' => ['required', 'digits:7'],
            'password' => [Rule::requiredIf(! $this->route('user')), 'confirmed'],
            'role' => ['required', 'integer', Rule::exists('roles', 'id')],
        ];
    }

    /**
     * Name regex rule
     * @return string
     */
    protected function nameRegexRule(): string
    {
        return 'regex:/^[\pL -]+$/u';
    }

    /**
     * @return \Illuminate\Validation\Rules\Unique
     */
    protected function uniqueRule()
    {
        return Rule::unique('users')->ignore($this->route('user'))->whereNull('deleted_at');
    }

    /**
     * @return array
     */
    protected function phoneCodes()
    {
        return array_merge(
            ...collect(Settings::get('mobile_operators', true))
                ->map(fn($operator) => $operator->codes)
                ->toArray()
        );
    }

    /**
     * Persist data
     * @param int|null $phoneId
     * @return array
     */
    public function data(int $phoneId = null): array
    {
        $data = array_filter($this->validationData(), function ($key) {
            return ! in_array($key, ['phone', 'role', 'password', 'password_confirmation']);
        }, ARRAY_FILTER_USE_KEY);

        $data['role_id'] = $this->input('role');
        $data['organization_id'] = 1;

        $phoneId && $data['phone_id'] = $phoneId;

        $this->input('password') && $data['password'] = Hash::make($this->input('password'));

        return $data;
    }
}
