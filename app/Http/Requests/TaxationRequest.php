<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

/**
 * Class TaxationRequest
 * @package App\Http\Requests
 */
class TaxationRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'regex:/^\w+$/', $this->uniqueRule()],
            'code' => ['required', 'digits:3', $this->uniqueRule()],
            'description' => 'regex:/^\w+$/'
        ];
    }

    /**
     * @return \Illuminate\Validation\Rules\Unique
     */
    protected function uniqueRule()
    {
        return Rule::unique('taxations')->ignore($this->route('taxation'))->whereNull('deleted_at');
    }
}
