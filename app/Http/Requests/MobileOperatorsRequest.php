<?php


namespace App\Http\Requests;


class MobileOperatorsRequest extends BaseRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'items' => ['required', 'array', 'min:1'],
            'items.*.name' => ['required', 'regex:/^[ \w-]+$/', 'distinct'],
            'items.*.codes' => ['required', 'array', 'min:1'],
            'items.*.codes.*' => ['required', 'integer', 'digits:3', 'distinct'],
        ];
    }
}