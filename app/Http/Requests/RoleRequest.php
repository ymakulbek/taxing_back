<?php

namespace App\Http\Requests;

use App\Repositories\Rights\Right;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                Rule::unique('roles')->ignore($this->route('role'))->whereNull('deleted_at')
            ],
            'rights' => ['required', 'array', 'min:1'],
            'rights.*.id' => ['required', 'integer', Rule::exists('rights', 'id')],
            'rights.*.actions' => ['required', 'array', 'min:1'],
            'rights.*.actions.*' => ['required', function ($attribute, $value, $fail) {
                preg_match('/^rights\.(\d+)\.actions\.\d+$/', $attribute, $matches);
                $actions = (new Right)->find($this->input("rights.{$matches[1]}.id"))->actions;

                ! in_array($value, $actions) && $fail('in');
            }],
            'default_right' => ['required', 'integer', Rule::in($this->input('rights.*.id'))]
        ];
    }

    /**
     * @return array
     */
    public function roleData(): array
    {
        $data = [
            'name' => $this->input('name'),
            'default_right_id' => $this->input('default_right'),
        ];

        ($description = $this->input('description')) && $data['description'] = $description;

        return $data;
    }

    /**
     * @param int $roleId
     * @return array
     */
    public function rightsData(int $roleId): array
    {
        return collect($this->input('rights'))->map(function ($right) use ($roleId) {
            return [
                'role_id' => $roleId,
                'right_id' => $right['id'],
                'actions' => json_encode($right['actions']),
            ];
        })->toArray();
    }
}
