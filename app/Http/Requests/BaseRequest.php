<?php


namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class BaseRequest
 * @package App\Http\Requests
 */
class BaseRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function validationData(): array
    {
        return array_filter(parent::validationData(), function ($key) {
            return $key !== '_method';
        }, ARRAY_FILTER_USE_KEY);
    }
}