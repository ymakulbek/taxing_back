<?php


namespace App\Facades\System;


use App\Repositories\Settings\Settings as SettingsRepository;

/**
 * Class Settings
 * @package App\Facades\System
 */
class SettingsWorker
{
    /**
     * @var SettingsRepository
     */
    private $repository;

    /**
     * Settings constructor.
     * @param SettingsRepository $repository
     */
    public function __construct(SettingsRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get setting by name
     * @param string $name
     * @param bool $decode
     * @return mixed
     */
    public function get(string $name, bool $decode = false)
    {
        $value = ($setting = $this->setting($name)) ? $setting->value : null;

        return $decode && $value && is_string($value)
            ? json_decode($value)
            : $value;
    }

    /**
     * Set setting by name/value
     * @param string $name
     * @param $value
     * @return bool
     */
    public function set(string $name, $value)
    {
        $setting = $this->setting($name);

        return $setting
            ? !! $this->repository->where('name', $name)->where('system', true)->update(['value' => $value])
            : !! $this->repository->insert(['name' => $name, 'value' => $value]);
    }

    /**
     * @param string $name
     * @return mixed
     */
    protected function setting(string $name)
    {
        return $this->repository->where('name', $name)->where('system', true)->first();
    }
}