<?php


namespace App\Facades\System;


use Illuminate\Support\Facades\Facade;

/**
 * Class SettingsFacade
 * @package App\Facades\System
 * @method static mixed get(string $name, bool $decode = false)
 * @method static bool set(string $name, $value)
 */
class Settings extends Facade
{
    /**
     * Get facade accessor
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'settings';
    }
}