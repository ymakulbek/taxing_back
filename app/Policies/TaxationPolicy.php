<?php


namespace App\Policies;


class TaxationPolicy extends Policy
{
    /**
     * @return bool
     */
    protected function index()
    {
        return $this->hasAction('index');
    }

    /**
     * @return bool
     */
    protected function rights()
    {
        return $this->store() || $this->update();
    }

    /**
     * @return bool
     */
    protected function store()
    {
        return $this->hasAction('store');
    }

    /**
     * @param int $id
     * @return bool
     */
    protected function update(int $id)
    {
        return $this->hasAction('update');
    }

    /**
     * @param int $id
     * @return bool
     */
    protected function destroy(int $id)
    {
        return $this->hasAction('destroy');
    }
}