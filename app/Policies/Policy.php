<?php


namespace App\Policies;


use App\Extensions\TaxingUser;
use Illuminate\Support\Str;

/**
 * Class Policy
 * @package App\Policies
 */
abstract class Policy
{
    /**
     * @var string
     */
    protected $resource;

    /**
     * @var TaxingUser
     */
    protected $user;

    /**
     * @param TaxingUser $user
     * @param mixed ...$args
     * @return bool
     */
    public function check(TaxingUser $user, ...$args): bool
    {
        $this->user = $user;

        if ($user->isSuperUser()) {
            return true;
        }

        if (empty($args) || ! $this->right()) {
            return false;
        }

        return $this->checkAction(...$args);
    }

    /**
     * @param string $action
     * @return bool
     */
    protected function hasAction(string $action)
    {
        return in_array($action, $this->right()->actions);
    }

    /**
     * Get right
     * @return mixed|null
     */
    protected function right()
    {
        if ($this->user->rights) {
            return collect($this->user->rights)->first(function ($right) {
                return $right->name === $this->getResource();
            });
        }
    }

    /**
     * Get policy resource
     * @return string
     * @throws \ReflectionException
     */
    protected function getResource(): string
    {
        if ($this->resource) {
            return $this->resource;
        }

        return $this->defaultResource();
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    protected function defaultResource(): string
    {
        preg_match('/^(\w+)Policy$/', object_class($this), $matches);

        return Str::plural(camel_to_snake($matches[1]));
    }

    /**
     * @param mixed ...$args
     * @return bool
     */
    private function checkAction(...$args): bool
    {
        $method = array_shift($args);

        $call = [$this, $method];

        return method_exists($this, $method) && (
            empty($args) ? call_user_func($call) : call_user_func_array($call, $args)
        );
    }
}