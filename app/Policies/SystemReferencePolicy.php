<?php


namespace App\Policies;


class SystemReferencePolicy extends Policy
{
    /**
     * @return bool
     */
    protected function index()
    {
        return $this->hasAction('index');
    }

    /**
     * @return bool
     */
    protected function mrp()
    {
        return $this->hasAction('mrp');
    }

    /**
     * @return bool
     */
    protected function mzp()
    {
        return $this->hasAction('mzp');
    }

    /**
     * @return bool
     */
    protected function mobileOperators()
    {
        return $this->hasAction('mobile_operators');
    }
}