<?php


namespace App\Extensions;


use App\Repositories\Serializable;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class TaxingUser extends GenericUser implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * @var Serializable
     */
    protected Serializable $user;

    /**
     * TaxingUser constructor.
     * @param Serializable $user
     */
    public function __construct(Serializable $user)
    {
        parent::__construct($user->getAttributes(true));

        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->getAuthIdentifier();
    }

    /**
     * @return mixed|void
     */
    public function jsonSerialize()
    {
        return $this->user->getAttributes();
    }

    /**
     * Check if user has superuser privileges
     * @return bool
     */
    public function isSuperUser()
    {
        return $this->user->role->name === 'superuser';
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->user->$name;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->user->$name = $value;
    }

    /**
     * @inheritDoc
     */
    public function hasVerifiedEmail()
    {
        // TODO: Implement hasVerifiedEmail() method.
    }

    /**
     * @inheritDoc
     */
    public function markEmailAsVerified()
    {
        // TODO: Implement markEmailAsVerified() method.
    }

    /**
     * @inheritDoc
     */
    public function sendEmailVerificationNotification()
    {
        // TODO: Implement sendEmailVerificationNotification() method.
    }

    /**
     * @inheritDoc
     */
    public function getEmailForVerification()
    {
        // TODO: Implement getEmailForVerification() method.
    }
}