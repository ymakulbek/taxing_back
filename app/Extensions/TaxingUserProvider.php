<?php


namespace App\Extensions;

use App\Repositories\GroupByCriteria;
use App\Repositories\Repository;
use App\Repositories\Rights\RightCriteria;
use App\Repositories\RoleRights\RoleRightCriteria;
use App\Repositories\Roles\RoleCriteria;
use App\Repositories\Serializable;
use App\Repositories\Users\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Hash;

class TaxingUserProvider implements UserProvider
{
    /**
     * Retrieve a user by their unique identifier.
     *
     * @param mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        return $this->getGenericUser(
            $this->withCriteria()->find($identifier)
        );
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param mixed $identifier
     * @param string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        $user = $this->getGenericUser(
            $this->withCriteria()->find($identifier)
        );

        return $user && $user->getRememberToken() && hash_equals($user->getRememberToken(), $token)
            ? $user : null;
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $this->repository()
            ->where($user->getAuthIdentifierName(), $user->getAuthIdentifier())
            ->update([$user->getRememberTokenName() => $token]);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        $credentials = array_filter($credentials, function ($key) {
            return $key !== 'password';
        }, ARRAY_FILTER_USE_KEY);

        if (empty($credentials)) {
            return null;
        }

        $repository = $this->withCriteria();

        foreach ($credentials as $key => $value) {
            is_array($value) ? $repository->whereIn($key, $value) : $repository->where($key, $value);
        }

        return $this->getGenericUser($repository->first());
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return Hash::check(
            $credentials['password'], $user->getAuthPassword()
        );
    }

    /**
     * Get the generic user.
     *
     * @param  mixed  $user
     * @return \Illuminate\Auth\GenericUser|null
     */
    protected function getGenericUser(Serializable $user = null)
    {
        if (! is_null($user)) {
            return new TaxingUser($user);
        }
    }

    /**
     * Get repository
     * @return Repository
     */
    protected function repository(): Repository
    {
        return new User;
    }

    /**
     * User repository with role criteria
     * @return Repository
     */
    protected function withCriteria(): Repository
    {
        return $this->repository()
            ->pushCriteria(new RoleCriteria)
            ->pushCriteria(
                (new RoleRightCriteria)->setLeftJoin('users_roles')
            )
            ->pushCriteria(
                (new RightCriteria)
                    ->setLeftJoin('users_roles')
                    ->setRightJoinAs('default_rights')
                    ->setForeignKey('default_right_id')
                    ->setSelectAs('default_right')
            )
            ->pushCriteria(GroupByCriteria::groups('users.id', 'default_rights.id'));
    }
}