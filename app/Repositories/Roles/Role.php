<?php


namespace App\Repositories\Roles;


use App\Repositories\Repository;

class Role extends Repository
{
    /**
     * @var array
     */
    protected $hiddenColumns = ['created_at', 'updated_at', 'deleted_at', 'default_right_id'];
}