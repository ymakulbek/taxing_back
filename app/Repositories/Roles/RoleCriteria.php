<?php


namespace App\Repositories\Roles;

use App\Repositories\JoinCriteria;
use Illuminate\Database\Query\Builder;

class RoleCriteria extends JoinCriteria
{

    /**
     * Join with filters
     * @param Builder $queryBuilder
     * @param Builder $subQuery
     */
    protected function filteredJoin(Builder $queryBuilder, Builder $subQuery): void
    {
        $queryBuilder->joinSub(
            $subQuery, $this->getRightJoinAs(), $this->first(), '=', $this->second()
        );
    }

    /**
     * Join without filters
     * @param Builder $queryBuilder
     */
    protected function ordinaryJoin(Builder $queryBuilder): void
    {
        $queryBuilder->join(
            "{$this->getRightJoin()} as {$this->getRightJoinAs()}", $this->first(), '=', $this->second()
        );
    }

    /**
     * Select
     * @return string
     */
    protected function select()
    {
        $table = $this->getRightJoinAs();

        return "
            json_build_object(
                'id', $table.id,
                'name', $table.name,
                'description', $table.description
            ) as {$this->getSelectAs()}
        ";
    }
}