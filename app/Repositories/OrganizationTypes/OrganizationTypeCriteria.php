<?php


namespace App\Repositories\OrganizationTypes;


use App\Repositories\JoinCriteria;
use Illuminate\Database\Query\Builder;

class OrganizationTypeCriteria extends JoinCriteria
{
    /**
     * @inheritDoc
     */
    protected function filteredJoin(Builder $queryBuilder, Builder $subQuery): void
    {
        $queryBuilder->joinSub($subQuery, $this->getRightJoinAs(), $this->first(), '=', $this->second());
    }

    /**
     * @inheritDoc
     */
    protected function ordinaryJoin(Builder $queryBuilder): void
    {
        $queryBuilder->join($this->as(), $this->first(), '=', $this->second());
    }

    /**
     * @return string
     */
    protected function select()
    {
        $table = $this->getRightJoinAs();

        return "
            json_build_object(
                'id', $table.id,
                'name', $table.name,
                'identification_number_type', $table.identification_number_type,
                'description', $table.description
            ) as {$this->getSelectAs()}
        ";
    }
}