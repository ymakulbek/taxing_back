<?php


namespace App\Repositories;


use Illuminate\Database\Query\Builder;

class GroupByCriteria implements Criteria
{
    /**
     * @var array
     */
    protected $groups;

    /**
     * GroupByCriteria constructor.
     * @param mixed array $groups
     */
    private function __construct(array $groups)
    {
        $this->groups = $groups;
    }

    /**
     * Apply criteria
     * @param Builder $queryBuilder
     * @param Repository $repository
     * @return Builder
     */
    public function apply(Builder $queryBuilder, Repository $repository): Builder
    {
        return $queryBuilder->groupBy($this->groups);
    }

    /**
     * Groups
     * @param mixed ...$groups
     * @return static
     */
    public static function groups(...$groups)
    {
        return new static($groups);
    }
}