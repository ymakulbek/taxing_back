<?php


namespace App\Repositories\Settings;


use App\Repositories\PostFetchHook;
use App\Repositories\Repository;
use App\Repositories\Serializable;

/**
 * Class Settings
 * @package App\Repositories\Settings
 */
class Settings extends Repository implements PostFetchHook
{
    /**
     * @var string
     */
    protected $table = 'settings';

    /**
     * @var bool
     */
    protected $softDelete = false;

    /**
     * @inheritDoc
     */
    public function applyHook(Serializable $data)
    {
        $data->value = json_decode($data->value);
    }
}