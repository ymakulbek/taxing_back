<?php


namespace App\Repositories\Users;

use App\Repositories\Repository;

class User extends Repository
{
    /**
     * @var array
     */
    protected $hiddenColumns = [
        'created_at',
        'updated_at',
        'deleted_at',
        'email_verified_at',
        'role_id',
        'phone_id',
        'organization_id',
        'password',
        'remember_token'
    ];
}