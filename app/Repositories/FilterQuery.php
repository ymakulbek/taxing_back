<?php


namespace App\Repositories;

use Illuminate\Database\Query\Builder;

/**
 * @method Repository|Criteria where(...$args)
 * @method Repository|Criteria orWhere(...$args)
 * @method Repository|Criteria whereIn(...$args)
 * @method Repository|Criteria whereNotIn(...$args)
 * @method Repository|Criteria orWhereIn(...$args)
 * @method Repository|Criteria orWhereNotIn(...$args)
 * @method Repository|Criteria whereNull(...$args)
 * @method Repository|Criteria whereNotNull(...$args)
 * @method Repository|Criteria orWhereNull(...$args)
 * @method Repository|Criteria orWhereNotNull(...$args)
 * @method Repository|Criteria whereDate(...$args)
 * @method Repository|Criteria whereMonth(...$args)
 * @method Repository|Criteria whereDay(...$args)
 * @method Repository|Criteria whereYear(...$args)
 * @method Repository|Criteria whereTime(...$args)
 * @method Repository|Criteria whereColumn(...$args)
 * @method Repository|Criteria orWhereColumn(...$args)
 */
trait FilterQuery
{
    /**
     * @var array
     */
    protected $wheres = [];

    /**
     * @param $name
     * @param $arguments
     * @return mixed|null
     */
    public function __call($name, $arguments)
    {
        if ($this->isWhere($name)) {
            $this->wheres[] = compact('name', 'arguments');

            return $this;
        }

        return null;
    }

    /**
     * Filter query
     * @param Builder $queryBuilder
     * @return Builder
     */
    protected function filterQuery(Builder $queryBuilder): Builder
    {
        foreach ($this->wheres as $where) {
            $method = $where['name'];

            method_exists($queryBuilder, $method) && call_user_func_array(
                array($queryBuilder, $method), $this->getArguments($where['arguments'])
            );
        }

        return $queryBuilder;
    }

    /**
     * Check if has filters
     * @return bool
     */
    protected function hasFilters(): bool
    {
        return !! count($this->wheres);
    }

    /**
     * Check if method is where
     * @param string $name
     * @return bool
     */
    private function isWhere(string $name): bool
    {
        return !! preg_match('/^\w*where\w*$/i', $name);
    }

    /**
     * Get formatted arguments
     * @param array $arguments
     * @return array
     */
    private function getArguments(array $arguments): array
    {
        return array_map(function ($argument, int $index) {
            if ($index) {
                return $argument;
            }

            return is_array($argument)
                ? $this->formatArrayArgument($argument)
                : $this->formatStringArgument($argument, $index);
        }, $arguments, array_keys($arguments));
    }

    /**
     * Format array argument
     * @param array $argument
     * @return array
     */
    private function formatArrayArgument(array $argument): array
    {
        ! is_array(reset($argument)) && $argument = [$argument];

        return array_map(function (array $item) {
            return array_map(function ($argumentItem, $index) {
                return $this->formatStringArgument($argumentItem, $index);
            }, $item, array_keys($item));
        }, $argument);
    }

    /**
     * Get argument in proper format
     * @param $argument
     * @param int $index
     * @return array|string
     */
    private function formatStringArgument($argument, int $index)
    {
        if ($index) {
            return $argument;
        }

        return $this->isColumn($argument) && $this instanceof Repository
            ? $this->getTable() . ".$argument"
            : $argument;
    }

    /**
     * Check if argument is column
     * @param string $argument
     * @return bool
     */
    private function isColumn(string $argument)
    {
        return !! preg_match('/^\w+$/', $argument);
    }
}
