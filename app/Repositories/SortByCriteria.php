<?php


namespace App\Repositories;


use Illuminate\Database\Query\Builder;

/**
 * Class SortByCriteria
 * @package App\Repositories
 */
class SortByCriteria implements Criteria
{
    /**
     * @var array
     */
    protected $sorts = [];

    /**
     * @var string
     */
    protected $table;

    /**
     * @var Repository
     */
    protected $repository;

    /**
     * GroupByCriteria constructor.
     * @param mixed array $groups
     */
    private function __construct(array $sorts)
    {
        $this->sorts = $sorts;
    }

    /**
     * Apply criteria
     * @param Builder $queryBuilder
     * @param Repository $repository
     * @return Builder
     */
    public function apply(Builder $queryBuilder, Repository $repository): Builder
    {
        $this->repository = $repository;

        $this->setOrders($queryBuilder);

        return $queryBuilder;
    }

    /**
     * Groups
     * @param mixed ...$sortParams
     * @return static
     */
    public static function sortBy(...$sortParams)
    {
        return new static(
            collect($sortParams)->filter(function ($param) { return !! $param; })->toArray()
        );
    }

    /**
     * Set orders
     * @param Builder $queryBuilder
     */
    protected function setOrders(Builder $queryBuilder)
    {
        foreach ($this->sorts as $sort) {
            $sortData = $this->sortData($sort);

            $queryBuilder->orderBy($sortData->column, $sortData->type);
        }
    }

    /**
     * Sort data from request param
     * @param string $sort
     * @return object
     */
    protected function sortData(string $sort)
    {
        $data = explode(':', $sort);

        return (object) [
            'column' => "{$this->getTable()}." . $data[0], 'type' => $data[1] ?? 'asc'
        ];
    }

    /**
     * Get table
     * @return string
     */
    public function getTable(): string
    {
        return isset($this->table) ? $this->table : $this->repository->getTable();
    }

    /**
     * @param string $table
     * @return SortByCriteria
     */
    public function setTable(string $table): SortByCriteria
    {
        $this->table = $table;

        return $this;
    }
}