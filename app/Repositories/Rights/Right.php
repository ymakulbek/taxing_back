<?php


namespace App\Repositories\Rights;


use App\Repositories\PostFetchHook;
use App\Repositories\Repository;
use App\Repositories\Serializable;

class Right extends Repository implements PostFetchHook
{
    /**
     * @var bool
     */
    protected $timestamps = false;

    /**
     * @var bool
     */
    protected $softDelete = false;

    /**
     * Apply post fetch hook to the data
     * @param $data
     */
    public function applyHook(Serializable $data)
    {
        $data->actions = json_decode($data->actions);
    }
}