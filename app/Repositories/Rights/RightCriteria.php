<?php


namespace App\Repositories\Rights;


use App\Repositories\JoinCriteria;
use Illuminate\Database\Query\Builder;

class RightCriteria extends JoinCriteria
{
    /**
     * Join with filters
     * @param Builder $queryBuilder
     * @param Builder $subQuery
     */
    protected function filteredJoin(Builder $queryBuilder, Builder $subQuery): void
    {
        $queryBuilder->leftJoinSub(
            $subQuery, $this->getRightJoinAs(), $this->first(), '=', $this->second()
        );
    }

    /**
     * Join without filters
     * @param Builder $queryBuilder
     */
    protected function ordinaryJoin(Builder $queryBuilder): void
    {
        $queryBuilder->leftJoin(
            $this->getRightJoin() . ' as ' . $this->getRightJoinAs(),
            $this->first(), '=', $this->second()
        );
    }

    /**
     * @return string
     */
    protected function select(): string
    {
        $table = $this->getRightJoinAs();

        return "
            case when $table.id is null then null 
                else json_build_object(
                    'id', $table.id,
                    'name', $table.name,
                    'description', $table.description,
                    'actions', $table.actions
                ) 
            end as {$this->getSelectAs()}
        ";
    }
}