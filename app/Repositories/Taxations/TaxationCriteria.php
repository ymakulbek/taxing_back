<?php


namespace App\Repositories\Taxations;


use App\Repositories\JoinCriteria;
use Illuminate\Database\Query\Builder;

class TaxationCriteria extends JoinCriteria
{
    /**
     * @inheritDoc
     */
    protected function filteredJoin(Builder $queryBuilder, Builder $subQuery): void
    {
        $queryBuilder->joinSub(
            $subQuery, $this->getRightJoinAs(), $this->first(), '=', $this->second()
        );
    }

    /**
     * @inheritDoc
     */
    protected function ordinaryJoin(Builder $queryBuilder): void
    {
        $queryBuilder->join(
            "{$this->getRightJoin()} as {$this->getRightJoinAs()}",
            $this->first(), '=', $this->second()
        );
    }

    /**
     * @return string
     */
    protected function select(): string
    {
        $table = $this->getRightJoinAs();

        return "
            json_build_object(
                'id', $table.id,
                'name', $table.name,
                'code', $table.code,
                'description', $table.description
            ) as {$this->getSelectAs()}
        ";
    }
}