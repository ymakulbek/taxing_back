<?php


namespace App\Repositories\RoleRights;


use App\Repositories\Repository;

class RoleRight extends Repository
{
    /**
     * @var bool
     */
    protected $timestamps = false;

    /**
     * @var bool
     */
    protected $softDelete = false;
}