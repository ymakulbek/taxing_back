<?php


namespace App\Repositories\RoleRights;


use App\Repositories\JoinCriteria;
use Illuminate\Database\Query\Builder;

class RoleRightCriteria extends JoinCriteria
{
    /**
     * RoleRightCriteria constructor.
     */
    public function __construct()
    {
        $this->setForeignKey('id');
        $this->setSelectAs('rights');
    }

    /**
     * Join with filters
     * @param Builder $queryBuilder
     * @param Builder $subQuery
     */
    protected function filteredJoin(Builder $queryBuilder, Builder $subQuery): void
    {
        $queryBuilder->leftJoinSub(
            $subQuery, $this->getRightJoinAs(), $this->first(), '=', $this->second('role_id')
        );

        $this->rightJoin($queryBuilder);
    }

    /**
     * Join without filters
     * @param Builder $queryBuilder
     */
    protected function ordinaryJoin(Builder $queryBuilder): void
    {
        $queryBuilder->leftJoin(
            $this->getRightJoin() . ' as ' . $this->getRightJoinAs(), $this->first(), '=', $this->second('role_id')
        );

        $this->rightJoin($queryBuilder);
    }

    /**
     * Right join
     * @param Builder $queryBuilder
     */
    protected function rightJoin(Builder $queryBuilder)
    {
        $queryBuilder
            ->leftJoin(
                'rights as rights_table',
                $this->getRightJoinAs() . '.right_id', '=', 'rights_table.id'
            )
            ->leftJoin(
                'right_groups', 'rights_table.group_id', '=', 'right_groups.id'
            )
            ->groupBy($this->first());
    }

    /**
     * Select
     * @return string
     */
    protected function select(): string
    {
        $table = $this->getRightJoinAs();

        return "
            case when count($table.id) = 0 then null
                else json_agg(json_build_object(
                    'id', rights_table.id,
                    'name', rights_table.name,
                    'group', json_build_object(
                        'name', right_groups.name,
                        'description', right_groups.description
                    ),
                    'actions', $table.actions,
                    'description', rights_table.description
                )) 
            end as {$this->getSelectAs()}
        ";
    }
}