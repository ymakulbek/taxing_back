<?php


namespace App\Repositories\TaxDepartments;


use App\Repositories\Repository;

class TaxDepartment extends Repository
{
    protected $hiddenColumns = [
        'created_at', 'updated_at', 'deleted_at', 'parent_id'
    ];
}