<?php


namespace App\Repositories\TaxDepartments;


use App\Repositories\JoinCriteria;
use Illuminate\Database\Query\Builder;

class TaxDepartmentCriteria extends JoinCriteria
{

    /**
     * Join with filters
     * @param Builder $queryBuilder
     * @param Builder $subQuery
     */
    protected function filteredJoin(Builder $queryBuilder, Builder $subQuery): void
    {
        $queryBuilder->leftJoinSub(
            $subQuery, $this->getRightJoinAs(), $this->first(), '=', $this->second()
        );
    }

    /**
     * Join without filters
     * @param Builder $queryBuilder
     */
    protected function ordinaryJoin(Builder $queryBuilder): void
    {
        $queryBuilder->leftJoin(
            "{$this->getRightJoin()} as {$this->getRightJoinAs()}", $this->first(), '=', $this->second()
        );
    }

    /**
     * Select
     * @return string
     */
    protected function select(): string
    {
        $table = $this->getRightJoinAs();

        return "
            case when {$table}.id is null then null 
                else json_build_object(
                    'id', $table.id,
                    'code', $table.code,
                    'bin', $table.bin,
                    'type', $table.type,
                    'name', $table.name
                )
            end as {$this->getSelectAs()}
        ";
    }
}