<?php


namespace App\Repositories;


use JsonSerializable;

class Serializable implements JsonSerializable
{
    /**
     * @var array
     */
    protected array $attributes;

    /**
     * @var array
     */
    protected array $hiddenAttributes;

    /**
     * Serializable constructor.
     * @param array $attributes
     * @param array $hiddenAttributes
     */
    public function __construct(array $attributes, array $hiddenAttributes = [])
    {
        $this->attributes = $attributes;
        $this->hiddenAttributes = $hiddenAttributes;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->getAttributes();
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->attributes[$name];
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * @param $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->attributes[$name]);
    }

    /**
     * @param array $hiddenAttributes
     * @return Serializable
     */
    public function setHiddenAttributes(array $hiddenAttributes): Serializable
    {
        $this->hiddenAttributes = $hiddenAttributes;

        return $this;
    }

    /**
     * @param string $name
     * @return Serializable
     */
    public function addHiddenAttribute(string $name): Serializable
    {
        $this->hiddenAttributes[] = $name;

        return $this;
    }

    /**
     * @param bool $withHidden
     * @return array
     */
    public function getAttributes(bool $withHidden = false): array
    {
        return $withHidden
            ? $this->attributes
            : array_filter(
                $this->attributes,
                function ($key) {
                    return ! in_array($key, $this->hiddenAttributes);
                },
                ARRAY_FILTER_USE_KEY
            );
    }
}