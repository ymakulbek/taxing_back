<?php


namespace App\Repositories;

use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use ReflectionException;

abstract class Repository
{
    use FilterQuery;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var string
     */
    protected $id = 'id';

    /**
     * @var bool
     */
    protected $timestamps = true;

    /**
     * @var array
     */
    protected $select = [];

    /**
     * @var array
     */
    protected $hiddenColumns = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * @var bool
     */
    protected $softDelete = true;

    /**
     * @var bool
     */
    protected $withTrashed = false;

    /**
     * @var bool
     */
    protected $onlyTrashed = false;

    /**
     * @var Collection
     */
    protected $criteria;

    /**
     * @var bool
     */
    protected $skipCriteria = false;

    /**
     * Get all records
     * @param array $columns
     * @return Collection
     */
    public function all($columns = ['*']): Collection
    {
        return $this->applyPostFetchHooks(
            $this->getQueryBuilder()->get($columns)
        );
    }

    /**
     * Paginate
     * @param int $page
     * @param int $perPage
     * @param array $columns
     * @return LengthAwarePaginator
     */
    public function paginate(int $page = 1, int $perPage = 15, $columns = ['*']): LengthAwarePaginator
    {
        $queryBuilder = $this->getQueryBuilder();

        $offset = ($page - 1) * $perPage;

        $items = $this->applyPostFetchHooks(
            $queryBuilder->offset($offset)->limit($perPage)->get($columns)
        );

        return new LengthAwarePaginator(
            $items, $queryBuilder->getCountForPagination(), $perPage, $page
        );
    }

    /**
     * Get first record
     * @param array $columns
     * @return mixed
     */
    public function first($columns = ['*'])
    {
        return $this->applyPostFetchHooks(
            $this->getQueryBuilder()->first($columns)
        );
    }

    /**
     * Find by column value
     * @param string $column
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy(string $column, $value, $columns = ['*'])
    {
        $queryBuilder = $this->getQueryBuilder();

        $column = $this->column($column);

        is_array($value) ? $queryBuilder->whereIn($column, $value) : $queryBuilder->where($column, $value);

        return $this->applyPostFetchHooks($queryBuilder->first($columns));
    }

    /**
     * Find by id
     * @param $idValue
     * @return mixed
     */
    public function find($idValue)
    {
        return $this->findBy($this->id, $idValue);
    }

    /**
     * Get items count
     * @return int
     */
    public function count()
    {
        return $this->getQueryBuilder()->count();
    }

     /**
     * Insert items
     * @param array $values
     * @return int|bool
     */
    public function insert(array $values)
    {
        $batch = is_array(reset($values));

        if ($batch) {
            return $this->batchInsert($values);
        }

        return $this->singleInsert($values);
    }

    /**
     * Batch insert
     * @param array $values
     * @return bool
     */
    protected function batchInsert(array $values): bool
    {
        $this->timestamps && $values = array_map(function ($item) {
            $item['created_at'] = Carbon::now();

            return $item;
        }, $values);

        return $this->getSimpleBuilder()->insert($values);
    }

    /**
     * Single insert
     * @param array $values
     * @return int
     */
    protected function singleInsert(array $values): int
    {
        $this->timestamps && $values['created_at'] = Carbon::now();

        return $this->getSimpleBuilder()->insertGetId($values);
    }

    /**
     * Update item
     * @param array $values
     * @return int
     */
    public function update(array $values)
    {
        $queryBuilder = $this->getQueryBuilder();

        $rowsUpdated = $queryBuilder->update($values);

        $rowsUpdated && $queryBuilder->update([
            $this->getTable() . '.updated_at' => Carbon::now()
        ]);

        return $rowsUpdated;
    }

    /**
     * Delete items
     * @param array|int $idValue
     * @return int
     */
    public function delete($idValue = null)
    {
        $queryBuilder = $idValue ? $this->getSimpleBuilder() : $this->getQueryBuilder();

        if ($idValue) {
            is_array($idValue)
                ? $queryBuilder->whereIn($this->id, $idValue)
                : $queryBuilder->where($this->id, $idValue);
        }

        return $this->softDelete
            ? $queryBuilder->update([$this->getTable() . '.deleted_at' => Carbon::now()])
            : $queryBuilder->delete();
    }

    /**
     * Push criterion
     * @param Criteria $criteria
     * @return Repository
     */
    public function pushCriteria(Criteria $criteria): Repository
    {
        (! $this->criteria) && $this->criteria = collect([]);

        $this->criteria->push($criteria);

        return $this;
    }

    /**
     * Set skip criteria flag
     * @param bool $skipCriteria
     * @return Repository
     */
    public function skipCriteria(bool $skipCriteria = true): Repository
    {
        $this->skipCriteria = $skipCriteria;

        return $this;
    }

    /**
     * Set skip criteria flag
     * @param bool $onlyTrashed
     * @return Repository
     */
    public function onlyTrashed(bool $onlyTrashed = true): Repository
    {
        $this->onlyTrashed = $onlyTrashed;

        return $this;
    }

    /**
     * Set skip criteria flag
     * @param bool $withTrashed
     * @return Repository
     */
    public function withTrashed(bool $withTrashed = true): Repository
    {
        $this->withTrashed = $withTrashed;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        if ($this->table) {
            return $this->table;
        }

        try {
            return camel_to_snake(object_class($this)) . 's';
        } catch (ReflectionException $e) {
            return null;
        }
    }

    /**
     * Apply post fetch hooks
     * @param $data
     * @return mixed
     */
    private function applyPostFetchHooks($data)
    {
        if ($data instanceof Collection) {
            return $data->map(function ($item) {
                return $this->applyHooks($item);
            });
        }

        return $this->applyHooks($data);
    }

    /**
     * Apply hooks
     * @param $data
     * @return mixed
     */
    private function applyHooks(&$data)
    {
        if ($data) {
            $data = new Serializable((array) $data, $this->hiddenColumns);

            $this instanceof PostFetchHook && $this->applyHook($data);

            $hooks = $this->postFetchHooks();

            $hooks && $hooks->each(function (PostFetchHook $hook) use ($data) {
                $hook->applyHook($data);
            });
        }

        return $data;
    }

    /**
     * Post fetch hooks
     * @return Collection
     */
    private function postFetchHooks()
    {
        if ($this->skipCriteria || ! $this->criteria) {
            return null;
        }

        return $this->criteria->filter(function ($criteria) {
            return $criteria instanceof PostFetchHook;
        });
    }

    /**
     * Get query builder
     * @return Builder
     */
    protected function getQueryBuilder(): Builder
    {
        $queryBuilder = $this->addSelect($this->getSimpleBuilder());

        $this->softDelete && $this->softDeleteCriteria($queryBuilder);

        $this->conditionsCriteria($queryBuilder);

        return $queryBuilder;
    }

    /**
     * Add select
     * @param Builder $queryBuilder
     * @return Builder
     */
    protected function addSelect(Builder $queryBuilder): Builder
    {
        if ( ! count($this->select)) {
            return $queryBuilder->addSelect($this->getTable() . '.*');
        }

        $select = array_map(function ($key) {
            return $this->getTable() . ".$key";
        }, array_diff($this->select, $this->hiddenColumns));

        return $queryBuilder->select($select);
    }

    /**
     * Get simple builder
     * @return Builder
     */
    protected function getSimpleBuilder(): Builder
    {
        return DB::table($this->getTable());
    }

    /**
     * Conditions criteria
     * @param Builder $queryBuilder
     */
    private function conditionsCriteria(Builder $queryBuilder)
    {
        $this->hasFilters() && $this->filterQuery($queryBuilder);

        if (! $this->skipCriteria) {
            $this->applyCriteria($queryBuilder);
        }
    }

    /**
     * @param Builder $queryBuilder
     */
    private function softDeleteCriteria(Builder $queryBuilder)
    {
        $deletedAt = $this->column('deleted_at');

        if ($this->onlyTrashed) {
            $queryBuilder->whereNotNull($deletedAt);
        } else {
            (! $this->withTrashed) && $queryBuilder->whereNull($deletedAt);
        }
    }

    /**
     * Apply criteria
     * @param Builder $queryBuilder
     */
    private function applyCriteria(Builder $queryBuilder)
    {
        $this->criteria && $this->criteria->each(function (Criteria $criteria) use ($queryBuilder) {
            $criteria->apply($queryBuilder, $this);
        });
    }

    /**
     * Table column
     * @param string $column
     * @return string
     */
    protected function column(string $column): string
    {
        $table = $this->getTable();

        return $table ? $table . ".$column" : $column;
    }

    /**
     * Get select fields
     * @return array
     */
    public function getSelect(): array
    {
        return $this->select;
    }

    /**
     * Set select fields
     * @param array $select
     * @return Repository
     */
    public function setSelect(array $select): Repository
    {
        $this->select = $select;

        return $this;
    }

    /**
     * @return string
     */
    public function sql(): string
    {
        return $this->getQueryBuilder()->toSql();
    }

    /**
     * @return array
     */
    public function bindings(): array
    {
        return $this->getQueryBuilder()->getBindings();
    }
}
