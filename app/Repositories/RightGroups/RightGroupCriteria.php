<?php


namespace App\Repositories\RightGroups;


use App\Repositories\JoinCriteria;
use Illuminate\Database\Query\Builder;

class RightGroupCriteria extends JoinCriteria
{
    /**
     * RightGroupCriteria constructor.
     */
    public function __construct()
    {
        $this->setForeignKey('group_id');
        $this->setSelectAs('group');
    }

    /**
     * Join with filters
     * @param Builder $queryBuilder
     * @param Builder $subQuery
     */
    protected function filteredJoin(Builder $queryBuilder, Builder $subQuery): void
    {
        $queryBuilder->joinSub(
            $subQuery, $this->getRightJoinAs(), $this->first(), '=', $this->second()
        );
    }

    /**
     * Join without filters
     * @param Builder $queryBuilder
     */
    protected function ordinaryJoin(Builder $queryBuilder): void
    {
        $queryBuilder->join(
            $this->getRightJoin() . ' as ' . $this->getRightJoinAs(),
            $this->first(), '=', $this->second()
        );
    }

    /**
     * Select
     * @return string
     */
    protected function select(): string
    {
        $table = $this->getRightJoinAs();

        return "
            json_build_object(
                'name', $table.name,
                'description', $table.description
            ) as {$this->getSelectAs()}
        ";
    }
}