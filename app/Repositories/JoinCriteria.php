<?php


namespace App\Repositories;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use ReflectionException;

abstract class JoinCriteria implements Criteria, PostFetchHook
{
    use FilterQuery;

    /**
     * @var Repository
     */
    protected Repository $repository;

    /**
     * @var string
     */
    private string $leftJoin;

    /**
     * @var string
     */
    private string $foreignKey;

    /**
     * @var string
     */
    private string $rightJoin;

    /**
     * @var string
     */
    private string $rightJoinAs;

    /**
     * @var string
     */
    private string $selectAs;

    /**
     * @param Builder $queryBuilder
     * @param Repository $repository
     * @return Builder
     */
    public function apply(Builder $queryBuilder, Repository $repository): Builder
    {
        $this->repository = $repository;

        $this->hasFilters()
            ? $this->filteredJoin($queryBuilder, $this->filterQuery(DB::table($this->getRightJoin())))
            : $this->ordinaryJoin($queryBuilder);

        method_exists($this, 'select') && $queryBuilder->addSelect(
            DB::raw($this->select())
        );

        return $queryBuilder;
    }

    /**
     * Join with filters
     * @param Builder $queryBuilder
     * @param Builder $subQuery
     */
    abstract protected function filteredJoin(Builder $queryBuilder, Builder $subQuery): void;

    /**
     * Join without filters
     * @param Builder $queryBuilder
     */
    abstract protected function ordinaryJoin(Builder $queryBuilder): void;

    /**
     * @param $data
     */
    public function applyHook(Serializable $data)
    {
        $selectAs = $this->getSelectAs();

        if (isset($data->$selectAs) && is_string($data->$selectAs)) {
            $data->$selectAs = json_decode($data->$selectAs);
        }

        method_exists($this, 'fetchHook') && $this->fetchHook($data);
    }

    /**
     * @return string
     */
    public function getForeignKey(): string
    {
        return $this->foreignKey ?? $this->defaultName() . '_id';
    }

    /**
     * @param string $foreignKey
     * @return JoinCriteria
     */
    public function setForeignKey(string $foreignKey): JoinCriteria
    {
        $this->foreignKey = $foreignKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getRightJoinAs(): string
    {
        return $this->rightJoinAs ?? Str::plural(
            $this->getLeftJoin() . '_' . $this->defaultName()
        );
    }

    /**
     * @param string $rightJoinAs
     * @return JoinCriteria
     */
    public function setRightJoinAs(string $rightJoinAs): JoinCriteria
    {
        $this->rightJoinAs = $rightJoinAs;

        return $this;
    }

    /**
     * @return string
     */
    public function getSelectAs(): string
    {
        return $this->selectAs ?? $this->defaultName();
    }

    /**
     * @param string $selectAs
     * @return JoinCriteria
     */
    public function setSelectAs(string $selectAs): JoinCriteria
    {
        $this->selectAs = $selectAs;

        return $this;
    }

    /**
     * @return string
     */
    public function as()
    {
        return $this->getRightJoin() . ' as ' . $this->getRightJoinAs();
    }

    /**
     * @return mixed
     */
    public function getLeftJoin()
    {
        return $this->leftJoin ?? $this->repository->getTable();
    }

    /**
     * @param string $leftJoin
     * @return JoinCriteria
     */
    public function setLeftJoin(string $leftJoin): JoinCriteria
    {
        $this->leftJoin = $leftJoin;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRightJoin()
    {
        return $this->rightJoin ?? Str::plural($this->defaultName());
    }

    /**
     * @param string $rightJoin
     * @return JoinCriteria
     */
    public function setRightJoin(string $rightJoin): JoinCriteria
    {
        $this->rightJoin = $rightJoin;

        return $this;
    }

    /**
     * @return string
     */
    protected function first(): string
    {
        return $this->getLeftJoin() . '.' . $this->getForeignKey();
    }

    /**
     * @param string $id
     * @return string
     */
    protected function second(string $id = 'id'): string
    {
        return $this->getRightJoinAs() . ".$id";
    }

    /**
     * Snake case criteria name
     * @return string
     */
    private function defaultName(): string
    {
        try {
            return camel_to_snake($this->criteriaName());
        } catch (ReflectionException $e) {
            return '';
        }
    }

    /**
     * Criteria name
     * @return string
     * @throws ReflectionException
     */
    private function criteriaName(): string
    {
        preg_match('/^(.+)Criteria$/', object_class($this), $matched);

        return $matched[1];
    }
}
