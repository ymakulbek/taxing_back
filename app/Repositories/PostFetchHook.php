<?php


namespace App\Repositories;


interface PostFetchHook
{
    /**
     * Apply post fetch hook to the data
     * @param $data
     */
    public function applyHook(Serializable $data);
}