<?php


namespace App\Repositories;


use Illuminate\Database\Query\Builder;

interface Criteria
{
    /**
     * Apply criteria
     * @param Builder $queryBuilder
     * @param Repository $repository
     * @return Builder
     */
    public function apply(Builder $queryBuilder, Repository $repository): Builder;
}