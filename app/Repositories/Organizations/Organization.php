<?php


namespace App\Repositories\Organizations;


use App\Repositories\Repository;

class Organization extends Repository
{
    protected $hiddenColumns = [
        'created_at',
        'updated_at',
        'deleted_at',
        'organization_type_id',
        'registration_department_id',
        'actual_department_id',
        'taxation_id',
    ];
}