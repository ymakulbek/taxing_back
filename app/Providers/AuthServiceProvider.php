<?php

namespace App\Providers;

use App\Extensions\TaxingUserProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * @var array
     */
    protected $gates = [
        'roles' => 'App\Policies\RolePolicy@check',
        'users' => 'App\Policies\UserPolicy@check',
        'tax-departments' => 'App\Policies\TaxDepartmentPolicy@check',
        'taxations' => 'App\Policies\TaxationPolicy@check',
        'system-references' => 'App\Policies\SystemReferencePolicy@check',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerGates();

        Passport::routes();

        Auth::provider('taxing', function () { return new TaxingUserProvider; });
    }

    /**
     * Register gates
     */
    protected function registerGates()
    {
        foreach ($this->gates as $resource => $policy) {
            Gate::define($resource, $policy);
        }
    }
}
