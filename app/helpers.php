<?php

if (! function_exists('camel_to_snake')) {
    /**
     * Camel case string to snake case
     * @param string $snakeCase
     * @return string|string[]|null
     */
    function camel_to_snake (string $snakeCase) {
        $function = function ($arg) {
            return '_' . strtolower($arg[1]);
        };

        $snakeCase[0] = strtolower($snakeCase[0]);

        return preg_replace_callback('/([A-Z])/', $function, $snakeCase);
    }
}

if (! function_exists('object_class')) {
    /**
     * Get object class name
     * @param $object
     * @return string
     * @throws ReflectionException
     */
    function object_class ($object) {
        return $className = (new ReflectionClass($object))->getShortName();
    }
}
