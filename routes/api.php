<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->namespace('API')->group(function () {
    // Auth
    Route::prefix('auth')->name('auth')->group(function () {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout')->middleware('auth:api');
        Route::get('user', 'AuthController@user')->middleware('auth:api');
    });

    // Resources
    Route::prefix('resources')->name('resources')->group(function () {
        Route::get('mobile-operators', 'UserController@mobileOperators')->name('users.mobileOperators');
        Route::get('taxations', 'UserController@taxations')->name('users.taxations');
        Route::get('tax-departments', 'UserController@taxDepartments')->name('users.taxDepartments');
    });

    // Registration
    Route::prefix('registration')->name('registration')->group(function () {
        Route::post('client', 'RegistrationController@client');
    });

    // Authorized actions
    Route::middleware(['auth:api', 'authorize'])->group(function () {
        Route::get('roles/rights', 'RoleController@rights')->name('roles.rights');
        Route::apiResource('roles', 'RoleController');

        Route::get('users/roles', 'RoleController@index')->name('users.roles');
        Route::apiResource('users', 'UserController')->except('show');

        Route::apiResource('tax-departments', 'TaxDepartmentController')->except('show');

        Route::apiResource('taxations', 'TaxationController')->except('show');

        Route::prefix('system-references')->group(function () {
            Route::get('', 'SystemReferenceController@index')->name('system-references.index');
            Route::put('mrp', 'SystemReferenceController@mrp')->name('system-references.mrp');
            Route::put('mzp', 'SystemReferenceController@mzp')->name('system-references.mzp');
            Route::put('mobile-operators', 'SystemReferenceController@mobileOperators')->name('system-references.mobileOperators');
        });
    });
});
